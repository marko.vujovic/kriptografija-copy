**Run with Flask**

`python app.py`


**PyCrypto lib**

On Fedora install with: 

`sudo dnf install python-crypto` 

(or `sudo yum install python-crypto` for older/other RPM distros)

On Ubuntu install with: 

`pip install pycrypto`


**Useful**

`inverse(u, v)` - Return the inverse of u modulo v.

`GCD(x,y)` - Return the greatest common divisor of x and y.

Links:
- https://www.dlitz.net/software/pycrypto/doc/
- https://www.dlitz.net/software/pycrypto/api/current/
- https://pypi.org/project/pycrypto/
- https://github.com/jaredks/pyprimesieve
- https://en.wikipedia.org/wiki/Primitive_root_modulo_n#Finding_primitive_roots
- https://codereview.stackexchange.com/questions/121862/fast-number-factorization-in-python
- https://github.com/cbohara/docker_for_flask_developers


**Docker**

Build image with :

`docker build -t kripto .`

Run image with:

`docker run -i -p 5000:5000 kripto`

Links:
- [Install Docker][install-docker].
- [Linux post-installation][linux-post-install].
- [Install Docker Compose][install-compose].





<!-- LINKOVI -->

[install-docker]: https://docs.docker.com/engine/installation/linux/docker-ce/centos/#install-docker-ce
[linux-post-install]: https://docs.docker.com/engine/installation/linux/linux-postinstall//
[install-compose]: https://docs.docker.com/compose/install/#install-compose