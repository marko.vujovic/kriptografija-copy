let msg_cnt = 1;

function generateParams() {
    const digitCount = +document.getElementById('digit-count').value;
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === XMLHttpRequest.DONE) {
           if (xhttp.status === 200) {
               const p = document.getElementById('p');
               const g = document.getElementById('g');
               const a = document.getElementById('a');
               const A = document.getElementById('capital-a');
               const resp = JSON.parse(xhttp.responseText);
               p.innerHTML = resp.p;
               g.innerHTML = resp.g;
               a.innerHTML = resp.a;
               A.innerHTML = resp.A;
               document.getElementById('digit-count').value = resp.digit_count;
               document.getElementById('digit-count').nextSibling.nextSibling.classList.add('active'); // ?
               document.getElementById('encrypt-panel').style.display = 'inline';
           }
           else {
               alert('Došlo je do greške');
           }
           document.getElementById('generate-loader').classList.remove('active');
        }
    };
    xhttp.open("GET", "parameters/" + digitCount, true);
    xhttp.send();
    document.getElementById('generate-loader').classList.add('active');
}

function encrypt() {
    const p = document.getElementById('p').innerHTML;
    const g = document.getElementById('g').innerHTML;
    const A = document.getElementById('capital-a').innerHTML;
    const m = document.getElementById('bob-m').value;
    const digitCount = document.getElementById('digit-count').value;
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === XMLHttpRequest.DONE) {
           if (xhttp.status === 200) {
                resp = JSON.parse(xhttp.responseText);
                document.getElementById('bob-encrypted-m').innerHTML = `(${resp[0]}, ${resp[1]})`;
                document.getElementById('send-panel').style.display = 'inline';
                document.getElementById('send-btn').style.display = 'inline';
           }
           else {
                document.getElementById('bob-encrypted-m').innerHTML = '';
                document.getElementById('send-panel').style.display = 'none';
                document.getElementById('send-btn').style.display = 'none';
                alert('Nepravilan unos');
           }
        }
    };
    
    data = {p: p, g: g, A: A, digit_count: digitCount, m: m};
    xhttp.open("POST", "encrypt", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(data));
}

function decrypt(msgCnt) {
    const p = document.getElementById('p').innerHTML;
    const g = document.getElementById('g').innerHTML;
    const a = document.getElementById('a').innerHTML;
    let encryptedMsg = document.getElementById(`alice-encrypted-m-${msgCnt}`).innerHTML.substr(1).slice(0, -1).split(',');
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === XMLHttpRequest.DONE) {
           if (xhttp.status === 200) {
               document.getElementById(`alice-decrypted-m-${msgCnt}`).innerHTML = escapeHTML(xhttp.responseText);
               document.getElementById(`decrypted-msg-panel-${msgCnt}`).style.display = 'block';
           }
           else {
               alert('Došlo je do greške');
           }
        }
    };
    data = {p: p, g: g, a: a, encryptedMsg: encryptedMsg};
    xhttp.open('POST', 'decrypt', true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify(data));
}

function send() {
    const encryptedMsg = document.getElementById('bob-encrypted-m').innerHTML;
    const decryptPanel = document.getElementById('decrypt-panel');
    const newMsg = createMsg();
    decryptPanel.appendChild(newMsg);
    decryptPanel.classList.remove('hidden');
    document.getElementById(`alice-encrypted-m-${msg_cnt}`).innerHTML = encryptedMsg;
    document.getElementById('g-sup-k').innerHTML = encryptedMsg.split(',')[0].substr(1);
    document.getElementById('m-A-sup-k').innerHTML = encryptedMsg.split(',')[1].slice(0, -1);
    msg_cnt++;
}

function parseFile(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = function(event) {
        document.getElementById('bob-m').value = event.target.result;
        M.textareaAutoResize(document.getElementById('bob-m'));
        document.getElementById('bob-m-label').classList.add('active');
    };
    reader.readAsText(file);
}

function downloadMessage() {
    const data = document.getElementById('bob-encrypted-m').innerHTML;
    download(data, 'encrypted-message.txt', 'text');
}

// Function to download data to a file
function download(data, filename, type) {
    const file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        const a = document.createElement('a');
        const url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

function deleteMsg(msgCnt) {
    const li = document.getElementById(`decrypt-panel-li-${msgCnt}`);
    li.parentElement.removeChild(li);
    const decryptPanel = document.getElementById('decrypt-panel');
    if (decryptPanel.childElementCount === 0) {
        decryptPanel.classList.add('hidden');
    }
}

function createMsg() {
    const html =
    `<li id="decrypt-panel-li-${msg_cnt}">
        <div class="collapsible-header"><i class="material-icons msg-icon">mail_outline</i></div>
        <div class="collapsible-body" id="decrypt-panel-body-${msg_cnt}"> 
            <div class="padding10">
                <label>Enkriptovana poruka</label> <br>
                <span id="alice-encrypted-m-${msg_cnt}" class="alice-encrypted-m"></span>
            </div>
            <button class="btn-small waves-effect waves-light deep-purple lighten-2" type="button" onclick="decrypt(${msg_cnt})">
                DECRYPT
            </button>
            <button class="btn-small bottom1px deep-purple lighten-2" onclick="deleteMsg(${msg_cnt})">
                <i class="material-icons">delete</i>
            </button>
            <div id="decrypted-msg-panel-${msg_cnt}" class="hidden padding10">
                <label>Dekriptovana poruka</label> <br>
                <span id="alice-decrypted-m-${msg_cnt}" class="alice-decrypted-m"></span>
            </div>
        </div>
    </li>`;
    return htmlToElement(html);
}

function htmlToElement(html) {
    const template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

function turnOnAliceMonitor() {
    document.getElementById('alice-monitor').classList.remove('off');
}

function turnOnBobMonitor() {
    document.getElementById('bob-monitor').classList.remove('off');
}

function clickOn(selector) {
    document.querySelector(selector).click();
}

window.onload = function() {
    const elems = document.querySelectorAll('.collapsible');
    M.Collapsible.init(elems);
};

function escapeHTML(str) {
    return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
}
