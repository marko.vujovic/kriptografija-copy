#-----------------------------IMPORTS----------------------------#
from math import log
from math import floor
from math import ceil
import sys
import itertools
from Crypto.Util import number
from Crypto.PublicKey import ElGamal
from Crypto.Util.number import GCD
from Crypto import Random
from Crypto.Random import random
from Crypto.Util.number import inverse
import string
#----------------------------------------------------------------#


#----------------------------FUNCTIONS---------------------------#
def elgam_enc(p, g, A, digit_count, plain_message):
	"""
	Takes keys and message as string value for input and encrypts it
	"""

	if plain_message is None or plain_message == "":
		raise ValueError("Incorrect input")

	# Removing non-ascii encodable characters
	printable = set(string.printable)
	plain_message = filter(lambda x: x in printable, plain_message)

	message_parts = split_message(plain_message, digit_count)
	k = random.randint(1, p-1)
	c1 = pow(g, k, p) 

	encrypted_message_parts = ""
	for message in message_parts:
		encrypted_message_parts += encrypt(p, g, A, k, message) + "."

	return [str(c1), encrypted_message_parts[:-1]]


def encrypt(p, g, A, k, message_part):
	"""
	Encrypts part of message with specified ElGamal parameters
	"""

	msg = prepare_message_for_encryption(message_part)
	encrypted_msg = (int(msg)*pow(A, k, p)) % p

	return str(encrypted_msg)


def split_message(message, digit_count):
	"""
	Splits message in parts whose lengths equals digit_count/3
	"""

	part_length = digit_count // 3
	message_parts = []

	for i in range(0, len(message), part_length):
		lower_bound = i
		upper_bound = min(len(message), i+part_length)

		message_parts.append(message[lower_bound: upper_bound])

	return message_parts


def prepare_message_for_encryption(plain_message):
	"""
	Takes string as input 
	Function converts each character into its ascii value and
	adds leading zeros so that each ascii has 3 digits
	"""

	m = ''
	m_array = filled_with_zeros(convert_string_to_nums(plain_message)[1])

	for elem in m_array:
		 m += elem	

	return m


def filled_with_zeros(nums_as_array):
	"""
	Takes array of numbers as input, and modifes each number with leading zeros,
	so that each number has 3 digits
	"""

	filled_nums_as_array = []

	for i in nums_as_array:
		if(i < 10):
			filled_nums_as_array.append('00' + str(i))
		elif(i < 100):
			filled_nums_as_array.append('0' + str(i))
		else:
			filled_nums_as_array.append(str(i))
			
	return filled_nums_as_array


def convert_string_to_nums(string):
	"""
	Converts each character of a string to it's ascii numerical value
	"""
	
	string_as_array = []
	string_as_nums = []
	
	for char in string:
		string_as_array.append(char)
		string_as_nums.append(ord(char))
	
	return [string_as_array, string_as_nums]


def elgam_dec(p, a, c_pair):
	"""
	Takes keys and crypthed pair as input, and returns decrypted message
	"""

	gk = c_pair[0]
	Ak = pow(int(gk), a, p)

	decrypted_message = ""
	message_parts = c_pair[1].split(".")
	
	for message in message_parts:
		decrypted_message += decryption(p, Ak, message)

	return decrypted_message


def decryption(p, Ak, message_part):
	"""
	Decrypts message with part with specified ElGamal parameters
	"""

	B = inverse(Ak, p)			
	result = (long(message_part) * B) % p

	return prepare_message_for_decription(str(result))


def prepare_message_for_decription(num):
	"""
	Takes number as input which is message in ascii format but with added zeros
	Function removes zeros and converts message to real string value
	"""

	ascii_array = ascii_with_zeros_to_ascii_list(num)
	m = ''.join(chr(i) for i in ascii_array)

	return m


def ascii_with_zeros_to_ascii_list(num):
	"""	
	Takes number as input which is message in ascii format but padded with zeros
	Function creates list of ascii values from that number
	"""

	string = str(num)
	m_array = []

	while (len(string)%3 != 0): 
		string = '0' + string
	
	for i in range(0,len(string)-2,3):
		m_array.append(int(string[i:i+3]))

	return m_array


def elgam_set(digit_count):
	"""
	Generates necessary parameters to start Elgamal algorithm
	"""
	
	if digit_count is None or digit_count < 3:
		digit_count = 3

	while 1:
		q=prim_num_find(digit_count)
		prime=2*q+1

		if number.isPrime(prime):
			break

	root = primitive_root_2(prime, q)
	a = random.StrongRandom().randint(1, prime-1)
	A = pow(root, a, prime)

	return [prime, root, a, A, str(prime), str(root), str(a), str(A), digit_count]


def prim_num_find(digit_count):
	"""
	Returns a prime number for specified digit length
	"""

	base_10 = 10**(digit_count-1)
	bit_length = ceil(log(base_10, 2)) + 1

	return number.getPrime(int(bit_length))


def primitive_root_2(prime, q):
	"""
	Returns random primitive root of a given prime
	"""

	while True:
		g = random.StrongRandom().randint(3, prime-1)
		if (pow(g, 2, prime)!=1) and (pow(g,q,prime)!=1):
			return g

	return None


def pseudo_primitive_root(prime):

	while True:
		k = random.StrongRandom().randint(1, prime-1)
		if GCD(k, prime-1) == 1:
			return k

	return None


def primitive_root(prime):
	"""
	Returns random primitive root of a given prime
	"""

	factors = prime_factors(prime-1)
	while True:
		k = random.StrongRandom().randint(1, prime-1)
		generator = True

		for j in factors:
			if  (pow(k, (prime - 1) / j, prime) == 1):
				generator = False
				break
			
		if generator:
			return k

	return None


#----------------------------UNUSED---------------------------#
def find_generator(prime):
	"""
	Returns generator of cyclic group for given prime
	"""

	factors = prime_factors(prime-1)
	for i in range(2, prime):
		generator = True
		# faktorisati prime-1 na proste faktore p_1,...p_k
		# za njih provjeriti da li je j ** (prime-1/p_i) = 1 mod prime
		# ako jeste, nije primitivni korijen
		
		for j in factors:
			if i**((prime - 1) / j) % prime == 1:
				generator = False
				break
			
		if generator:
			return i


def primRoot(theNum):
    o = 1
    r = 2
    while r < theNum:
        k = pow(r, o, theNum)
        while (k > 1):
            o = o + 1
            k = (k * r) % theNum
        if o == (theNum - 1):
            return r
        o = 1
        r = r + 1

    return None


def prime_factors(n):
	"""
	Returns list of prime factors
	"""

	result = []
	for i in prime_factors_generator(n):
		result.append(i)
	
	return result


def prime_factors_generator(n):
	"""
	Returns generator of prime factors
	"""

	for i in itertools.chain([2], itertools.count(3, 2)):
		if n <= 1:
			break
		while n % i == 0:
			n //= i
		yield i


def prim_num_find_old(digit_count):
	"""
	Returns a prime number for specified digit length
	Inefficient for large inputs
	"""
	
	start_num = 10**(digit_count-1) + 1
	end_num = 10**(digit_count)
	
	for num in range(start_num, end_num, 2):
		if miller_rabin(num):
			return num
	
	
def miller_rabin(n):
	"""
	Primality test - Deterministic version
	Returns True if prime
	Too slow
	"""

	if n == 1:
		return None
	elif n == 2:
		return True
	elif n % 2 == 0:
		return False
	
	# Faktorisem n-1 kao 2^s*d
	m = n-1
	for s in range(1, n-1):
		if m % (2**s) == 0:
			d = m // (2**s)
			break
			
	upper_bound = min(m, floor(2 * (log(n)**2)))
	for a in range(2, upper_bound):
		if a**d % n == 1:
			continue

		next = False
		for r in range(0, s):
			if a**(2**r * d) % n == m:
				next = True
				break

		if not(next):
			return False
			
	return True
#----------------------------------------------------------------#


#-------------------------------MAIN-----------------------------#
# digit_count = 50
# string = "0123456789012345678901234567890123456789012345678901234567890123456789qwertzuiopasdfghjklyxcvbnm"

# elGamSet = elgam_set(digit_count)
# p = elGamSet[0]
# g = elGamSet[1]
# a = elGamSet[2]
# A = elGamSet[3]

# print(p)
# print(g)
# print(a)
# print(A)

# print(elgam_enc(p, g, A, digit_count, string))
# res = elgam_dec(p, a, elgam_enc(p, g, A, digit_count, string))
# print(string)
# print(res)
# print(res == string)