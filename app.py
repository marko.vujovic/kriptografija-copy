import functools

from flask import Flask, render_template, jsonify, request, json
import el_gamal

app = Flask(__name__)


def return_json(f):
    @functools.wraps(f)
    def inner(*a, **k):
        return json.dumps(f(*a, **k))
    return inner


@app.route('/')
def homepage():
    return render_template("homepage.html")


@app.route('/parameters/<int:digits>')
def generate_parameters(digits):
    x = el_gamal.elgam_set(digits)
    return jsonify(p=x[4], g=x[5], a=x[6], A=x[7], digit_count=x[8])


@app.route('/encrypt', methods=['POST'])
@return_json
def encrypt():
    x = request.json
    return el_gamal.elgam_enc(int(x['p']), int(x['g']), int(x['A']), int(x["digit_count"]), x['m'])


@app.route('/decrypt', methods=['POST'])
def decrypt():
    x = request.json
    return el_gamal.elgam_dec(int(x['p']), int(x['a']), [int(x['encryptedMsg'][0]), x['encryptedMsg'][1]])


if __name__ == '__main__':
    # app.run(debug=False, host='0.0.0.0')
    app.run(debug=True, host='0.0.0.0')